﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Juvenile } from '../_models';

@Injectable()
export class JuvenileService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Juvenile[]>(`${config.apiUrl}/users`);
    }

    getById(id: number) {
        return this.http.get(`${config.apiUrl}/users/` + id);
    }

    register(juvenile: Juvenile) {
        return this.http.post(`${config.apiUrl}/juvenile/register`, juvenile);
    }

    update(juvenile: Juvenile) {
        return this.http.put(`${config.apiUrl}/users/` + juvenile.id, juvenile);
    }

    delete(id: number) {
        return this.http.delete(`${config.apiUrl}/users/` + id);
    }

    
}
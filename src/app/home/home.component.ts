﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../_models';
import { UserService } from '../_services';

import { AlertService, JuvenileService } from '../_services';
import { Router } from '@angular/router';
import { CommonModule } from "@angular/common";

@Component({
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    currentUser: any;
    users: any;
    show_add_user: boolean = false;
    show_juvenile_list: boolean = false;
    selectedTab: number = 1;

    registerForm: FormGroup;
    registerFormStep2: FormGroup;
    loading = false;
    submitted = false;
    final_submitted = false

    juvenileUsers: any;

    constructor(
        private userService: UserService,
        private formBuilder: FormBuilder,
        private router: Router,
        private juvenileService: JuvenileService,
        private alertService: AlertService) {

        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.users = JSON.parse(localStorage.getItem('users'));
        this.juvenileUsers = JSON.parse(localStorage.getItem('juveniles'));
    }


    ngOnInit() {
        //  this.loadAllUsers();
        this.registerForm = this.formBuilder.group({
            countryJid: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            sid: [],
            assignedjudge: [],
        });

        this.registerFormStep2 = this.formBuilder.group({
            height: ['', Validators.required],
            gender: [],
            eyecolor: []
        });

        // this.users = JSON.parse(localStorage.getItem('users'));
        this.juvenileUsers = JSON.parse(localStorage.getItem('juveniles'));
    }

    deleteUser(id: number) {
        this.userService.delete(id).pipe(first()).subscribe(() => {
            this.loadAllUsers()
        });
    }

    private loadAllUsers() {
        this.userService.getAll().pipe(first()).subscribe(users => {
            // this.users = users;
        });
    }

    addJuvenile() {
        this.show_add_user = true;
        this.show_juvenile_list = false;
        this.selectedTab = 0;
    }

    listJuvenile() {
        this.show_add_user = false;
        this.show_juvenile_list = true;
        this.selectedTab = 0;
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }
    get f2() { return this.registerFormStep2.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.selectedTab = 1;
    }

    onNext() {

    }

    changeTab(event:any) { 
        this.selectedTab =  event.index;
    }

    onFinalSubmit() {
        this.final_submitted = true;

        // stop here if form is invalid
        if (this.registerFormStep2.invalid) {
            return;
        }

        if (this.registerForm.invalid) {
            this.submitted = true;
            this.selectedTab = 0;
            return;
        }

        const allformValue = { ...this.registerForm.value, ...this.registerFormStep2.value }
        this.juvenileService.register(allformValue)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Juvenile added  successfully', false);
                    this.loading = false;
                    this.juvenileUsers = JSON.parse(localStorage.getItem('juveniles'));
                    setTimeout(()=> this.alertService.clear(),8000);
                    this.submitted = false;
                    this.final_submitted = false;
                    this.registerForm.reset();
                    this.registerForm.clearValidators(); 
                    this.registerFormStep2.reset();
                    this.registerFormStep2.clearValidators(); 
                    this.selectedTab = 0;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });


    }
}
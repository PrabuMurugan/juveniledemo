﻿export class Juvenile {
    id: number;
    countryJid: string;
    firstName: string;
    lastName: string;
    sid: string;
    assignedjudge:string;
}